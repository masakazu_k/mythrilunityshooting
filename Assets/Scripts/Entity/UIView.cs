﻿using UnityEngine;
using UnityEngine.UI;

public class UIView : CharaView
{
    // スコア
    [SerializeField] private GameObject score;
    private Text scoreText;
    // ハイスコア
    [SerializeField] private GameObject highscore;
    private Text highscoreText;
    // 時間
    [SerializeField] private GameObject timer;
    private Text timerText;
    // 残弾数
    [SerializeField] private GameObject amo;
    private Text amoText;
    // HP
    [SerializeField] private GameObject life;
    private Text lifeText;
    // クロスヘア
    [SerializeField] private GameObject crosshair;
    private Text crosshairText;
    // カウントダウン
    [SerializeField] private GameObject count;
    private Text countText;
    // 装備名
    [SerializeField] private GameObject weapon;
    private Text weaponText;

    // ゲーム中UI群
    private GameObject[] ui;

    [HideInInspector] public ScoreModel scoreModel;
    [HideInInspector] public PlayerModel playerModel;

    private void Awake()
    {
        // カウントダウン以外をゲーム中UI群に登録
        ui = new GameObject[] {
            score,
            highscore,
            timer,
            amo,
            life,
            crosshair,
            weapon
        };

        // 全て非表示
        foreach (var elm in ui) elm.transform.localScale = Vector3.zero;
        count.transform.localScale = Vector3.zero;

        scoreText = score.GetComponent<Text>();
        highscoreText = highscore.GetComponent<Text>();
        timerText = timer.GetComponent<Text>();
        amoText = amo.GetComponent<Text>();
        lifeText = life.GetComponent<Text>();
        crosshairText = crosshair.GetComponent<Text>();
        countText = count.GetComponent<Text>();
        weaponText = weapon.GetComponent<Text>();
    }

    public override void ApplyModel()
    {
        var t = Time.timeSinceLevelLoad;

        // 開始8秒までの処理
        if (t < 8.0f)
        {
            // 秒数の小数を切り落とす
            var countIdx = Mathf.FloorToInt(t);
            switch (countIdx)
            {
                // 7-8秒
                case 7:
                    // すべての全てのUIをゲーム中表示にする
                    SetUIScale();
                    SetCountScale();
                    break;
                // 0-4秒
                case 3:
                case 2:
                case 1:
                case 0:
                    // カウントダウンテキストを設定する
                    SetCount(countIdx);
                    // テキストのアニメーションを適用する
                    SetCountScale(Mathf.Clamp01(t - (countIdx - 0.5f)));
                    break;
            }
        }
        // スコア
        SetScore(scoreModel.totalScore, scoreModel.IsOverHighScore ? "#ffcc00" : "white");
        SetHighscore(scoreModel.GetHighScore(), scoreModel.IsOverHighScore ? "#ffcc00" : "white");
        // タイマーの表示を更新する
        SetTimer(t - 6.0f);
        // 残弾
        if (playerModel.activeEquip is GunModel)
        {
            SetAmo(
                (playerModel.activeEquip as GunModel)?.amoRemain ?? 0,
                (playerModel.activeEquip as GunModel).amoMax,
                playerModel.magagines[(int)(playerModel.activeEquip as GunModel).type]
            );
            if ((playerModel.activeEquip as GunModel).reloadCompleteTime != null) SetAmoReloading();
        }
        else if (playerModel.activeEquip is GrenadeModel)
        {
            SetAmo(
                (playerModel.activeEquip as GrenadeModel)?.amoRemain ?? 0,
                0
            );
        }
        else
        {
            amoText.text = "<color=red>0/0</color>";
        }
        // クロスヘア
        DisplayCrosshair(playerModel.isFirstView);
        SetCrosshair((playerModel.activeEquip as GunModel)?.castCompleteTime != null);
        // HP
        SetLife(playerModel.life);
        SetWeaponName(playerModel.activeEquipIndex + 1, playerModel.equipMax, playerModel.activeEquip?.name);
    }

    // 値から色を取得
    private string GetValueColor(int value, int max = 10)
    {
        switch (max == 10 ? value : Mathf.FloorToInt(value * 10 / max))
        {
            // 0%
            case 0: return "#800000";
            // 10%
            case 1: return "red";
            // 20%
            case 2: return "yellow";
            // >20%
            default: return "white";
        }
    }

    // UI群の表示倍率を設定する
    public void SetUIScale(float scale = 1.0f)
    {
        foreach (var elm in ui) elm.transform.localScale = new Vector3(scale, scale, 1.0f);
    }
    // カウントダウンの表示倍率を設定する
    public void SetCountScale(float scale = 0.0f) => count.transform.localScale = new Vector3(scale, scale, 1.0f);
    // スコアを設定する
    public void SetScore(int value, string color = "white") => scoreText.text = $"Score: <color={color}>{value}</color>";
    // ハイスコアを設定する
    public void SetHighscore(int value, string color = "white") => highscoreText.text = $"HighScore: <color={color}>{value}</color>";
    // 0埋めの長さ
    private const int padLen = 2;
    // 0埋め文字
    private const char padChar = '0';
    // 実数を指定長0埋め整数文字列に
    private System.Func<float, string> formatter = v => Mathf.FloorToInt(v).ToString().PadLeft(padLen, padChar);
    // タイムを設定する
    public void SetTimer(float value, string color = "white") => timerText.text = $"<color={color}>{formatter(value / 60.0f)}:{formatter(value % 60.0f)}</color>";

    // 残弾数をリロード中文字列に設定する
    public void SetAmoReloading() => amoText.text = $"Reloading...";
    // 残弾数を設定する
    public void SetAmo(int value, int max, int magagines = 0) => amoText.text = $"<color={GetValueColor(value)}>{value}</color>/{max}[{magagines}]";
    // HPを設定する
    public void SetLife(int value) => lifeText.text = $"HP: <color={GetValueColor(value)}>{value}</color>";
    // クロスヘアの表示を制御する
    public void DisplayCrosshair(bool isShow = true) => crosshair.SetActive(isShow);
    // クロスヘアのカラーを設定する
    public void SetCrosshair(bool isCooling) => crosshairText.text = $"<color={(isCooling ? "red" : "green")}>+</color>";
    // カウントダウンの各文字列
    private string[] countMes = { "3", "2", "1", "Start" };
    // カウントダウンを設定する
    public void SetCount(int index, string color = "white") => countText.text = $"<color={color}>{countMes[index]}</color>";
    public void SetWeaponName(int index, int indexMax, string name) => weaponText.text = $"{index}/{indexMax} - {(name == null ? "none" : name)}";
}
