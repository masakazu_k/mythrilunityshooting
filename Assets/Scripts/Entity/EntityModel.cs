﻿using UnityEngine;

public abstract class EntityModel
{
    public int id = -1;

    public ColliderModel collider = null;

    public abstract event System.Action<EntityModel> OnDestroy;
    public virtual event System.Action<EntityModel> OnHit;

    public virtual Vector3 position { get { return collider?.position ?? Vector3.zero; } set { if (collider != null) collider.position = value; } }
    public virtual Quaternion rotation { get { return collider?.rotation ?? Quaternion.identity; } set { if (collider != null) collider.rotation = value; } }
    public virtual Vector3 velocity { get { return collider?.velocity ?? Vector3.zero; } set { if (collider != null) collider.velocity = value; } }
    public virtual Vector3 scale { get { return collider?.scale ?? Vector3.zero; } set { if (collider != null) collider.scale = value; } }

    // 重力値
    protected static Vector3 gravity = new Vector3(0.0f, -9.81f, 0.0f);
    public float mass = 1.5f;

    public EntityModel DeadBy;
    public virtual bool IsDead { get; protected set; }
    public virtual void Destroy(EntityModel caused) => IsDead = true;
    public abstract void Update();
    public bool IsGroundedObstacle = false;
    public virtual bool IsGrounded => position.y <= collider.scale.y * 0.5f || IsGroundedObstacle;
    public virtual void UpdatePosition()
    {
        if (collider == null) return;

        if (!IsGrounded) collider.velocity += gravity * mass * 0.016f;
        else if (collider.velocity.y < 0) collider.velocity.y = 0;

        collider.position += collider.velocity * 0.016f;

        IsGroundedObstacle = false;
    }
    public virtual void Hit(EntityModel target)
    {
        var reflectionForce = -target.collider.GetReflectionForceColliders(collider);

        if (reflectionForce.y > 0) IsGroundedObstacle = true;
        if (reflectionForce.x != 0 || reflectionForce.z != 0) position += reflectionForce;

        OnHit?.Invoke(target);
    }
}
