﻿using UnityEngine;

public class ObstacleModel : EntityModel
{
    public ObstacleModel(float fieldSize)
    {
        collider = new CubeColliderModel();

        collider.scale.Set(
            Random.Range(2, 30),
            Random.Range(2, 30),
            Random.Range(2, 30)
        );
        collider.position.Set(
            Random.Range(-fieldSize, fieldSize),
            Mathf.Pow(Random.Range(0, 1.0f), 16) * collider.scale.y * 0.5f,
            Random.Range(-fieldSize, fieldSize)
        );
        collider.rotation = Quaternion.Euler(
            0,
            Random.Range(0, 90),
            0
        );
    }

    public override bool IsDead => false;

    public override event System.Action<EntityModel> OnDestroy;

    public override void Update() { }

    public override void Hit(EntityModel target) { }
}
