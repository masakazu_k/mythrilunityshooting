﻿using UnityEngine;
using UnityEngine.UI;

/**
 * 開始タイミングの同期を自前でやらないとずれそう
 */

public class MatchingSceneBehavior : SceneBehavior
{
    private PhotonView photonView;
    private int seed;
    private bool isJoined = false;
    private Text stateText;

    private void Start()
    {
        photonView = GetComponent<PhotonView>();
        stateText = GameObject.Find("State").GetComponent<Text>();
        SetState();

        // カーソルロックを解除
        Cursor.lockState = CursorLockMode.None;
        PhotonNetwork.ConnectUsingSettings(null);
    }

    public void OnJoinedLobby()
    {
        SetState($"Lobby: {PhotonNetwork.playerList.Length} Players active");
        PhotonNetwork.JoinOrCreateRoom(
            "Room",
            new RoomOptions()
            {
                CustomRoomProperties = new ExitGames.Client.Photon.Hashtable() {
                    { "seed", (int)System.DateTime.Now.Ticks },
                    { "isPlaying", false },
                    { "startDate", 0 }
                }
            },
            null);
    }

    private void UpdateStateText() => SetState($"Room: {PhotonNetwork.room.Name} [{PhotonNetwork.room.PlayerCount}/{PhotonNetwork.room.MaxPlayers}] {(PhotonNetwork.isMasterClient ? " - You are Master | Click to Start." : " | Wating to Start By Master.")}");

    // Update is called once per frame
    public void OnJoinedRoom()
    {
        UpdateStateText();
        Random.InitState((int)PhotonNetwork.room.CustomProperties["seed"]);
        isJoined = true;
    }
    public void OnPhotonPlayerDisconnected() => UpdateStateText();
    public void OnPhotonPlayerConnected() => UpdateStateText();

    private long startTime = -1;
    private void Update()
    {
        if (isJoined)
        {
            if (!(bool)PhotonNetwork.room.CustomProperties["isPlaying"])
            {
                if (PhotonNetwork.isMasterClient && Input.GetMouseButtonDown(0))
                {
                    PhotonNetwork.room.SetCustomProperties(new ExitGames.Client.Photon.Hashtable() {
                        { "isPlaying", true },
                        { "startDate", System.DateTime.Now.Ticks }
                    });
                    isJoined = false;
                    SetState("Starting");
                    photonView.RPC("SetStartTime", PhotonTargets.AllBufferedViaServer, System.DateTime.Now.Ticks + 100);
                }
            }
        }
        if (startTime > 0 && startTime <= System.DateTime.Now.Ticks) SetNextScene();
    }

    private void SetState(string val = "") => stateText.text = val;

    [PunRPC]
    private void SetStartTime(long time) => startTime = time;

    [PunRPC]
    private void GoToNextScene() => SetNextScene();
}
