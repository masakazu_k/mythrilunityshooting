﻿using UnityEngine;

public abstract class EquippableModel : EntityModel
{
    public string name = "Unnamed";

    public CharaModel owner { get; protected set; } = null;

    public override event System.Action<EntityModel> OnDestroy;
    public virtual event System.Action<EntityModel> OnAction;

    public abstract void Action();
    public float globalForward = 0.5f;
    public override Vector3 position
    {
        get
        {
            if (owner == null) return base.position;
            else return owner.lookOrigin + (owner is PlayerModel ? (owner as PlayerModel).look : owner.rotation) * (Vector3.forward * globalForward + Vector3.down * 0.2f) + base.position;
        }

        set
        {
            base.position = value;
        }
    }
    public override Quaternion rotation
    {
        get
        {
            if (owner == null) return base.rotation;
            else return (owner is PlayerModel ? (owner as PlayerModel).look : owner.rotation) * base.rotation;
        }

        set
        {
            base.rotation = value;
        }
    }

    public float suisideTime = -1; // 所有者がいない時の自動消滅時刻
    public const float suisideGraceTime = 100.0f; // 自動消滅までの時間[sec]

    public float recastTime = 1.0f;
    public float scopeLevel = 1.0f;

    public override void Destroy(EntityModel caused) => IsDead = true;

    public EquippableModel() { collider = new SphereColliderModel(); }

    public virtual void Eject()
    {
        if (owner == null) return;

        position = position + rotation * Vector3.forward;
        rotation = rotation;

        owner = null;
        suisideTime = Time.timeSinceLevelLoad + suisideGraceTime;
    }

    public virtual void Equip(CharaModel newOwner)
    {
        Eject();
        if (newOwner == null) return;

        position = Vector3.zero;
        rotation = Quaternion.identity;

        owner = newOwner;
        suisideTime = -1;
    }

    public override void Hit(EntityModel target)
    {
        if (owner != null) return;

        base.Hit(target);
    }

    public override void Update()
    {
        if (owner == null) UpdatePosition();
        if (suisideTime > 0 && suisideTime < Time.timeSinceLevelLoad) IsDead = true;
    }
}
