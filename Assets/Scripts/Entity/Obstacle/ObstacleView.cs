﻿using UnityEngine;

public class ObstacleView : EntityView
{
    public override void ApplyModel()
    {
        base.ApplyModel();
        transform.localScale = model.scale;
    }
}
