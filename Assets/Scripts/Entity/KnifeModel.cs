﻿using UnityEngine;

public class KnifeModel : EquippableModel
{
    public override event System.Action<EntityModel> OnAction;
    public EntityModel caster;

    public KnifeModel()
    {
        name = "Knife";
        globalForward = 1.0f;
        collider.rotation.SetLookRotation(Vector3.left);
    }

    public float attackTime = 0.5f;
    public float activateTime = -1;

    public void Activate() => activateTime = Time.timeSinceLevelLoad;

    public float? startAttack = 0;
    public override void Action()
    {
        startAttack = Time.timeSinceLevelLoad;
        var knife = new KnifeModel();
        knife.collider.position = position;
        knife.collider.rotation = rotation;
        knife.collider.velocity = (owner is PlayerModel ? (owner as PlayerModel).look : owner.rotation) * Vector3.forward * 10.0f + Vector3.up * 10.0f + owner.velocity;
        knife.Activate();
        knife.caster = owner;
        knife.SetOwner(owner);
        OnAction?.Invoke(knife);
    }

    public void SetOwner(CharaModel newOwner) => owner = newOwner;

    public override void Update()
    {
        if (activateTime > 0 && activateTime + attackTime < Time.timeSinceLevelLoad)
            Destroy(null);
        if (owner == null && caster == null)
            base.Update();
    }

    public override void Hit(EntityModel target) { }
}
