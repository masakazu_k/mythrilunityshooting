﻿using System.Collections.Generic;
using UnityEngine;

public class ScoreModel
{
    public int id;

    // ハイスコア記録用キー
    private const string highscoreKey = "HIGH_SCORE";
    // スコア記録用キー
    private const string scoreKey = "SCORE";

    private List<EntityModel> entityList;
    private PlayerModel player;
    // ハイスコア
	private int highScore;

    public ScoreModel(PlayerModel playerModel, List<EntityModel> entityModels)
    {
        player = playerModel;
        entityList = entityModels;
        
        // ハイスコアを取得
        highScore = PlayerPrefs.GetInt(highscoreKey, 0);
    }

    // 破棄処理
    ~ScoreModel()
    {
        // スコアを保存
        PlayerPrefs.SetInt(scoreKey, player.score);
        // ハイスコアが更新されていたら、ハイスコアを保存
        if (player.score > highScore) PlayerPrefs.SetInt(highscoreKey, player.score);
    }

    // スコアを取得する
    public int GetScore() => player.score;

    public int GetHighScore() => highScore;
    public bool IsOverHighScore => player.score > highScore;
    public int totalScore
    {
        get
        {
            var sum = 0;

            foreach (PlayerModel p in entityList.FindAll(e => e is PlayerModel)) sum += p.score;

            return sum;
        }
    }
}
