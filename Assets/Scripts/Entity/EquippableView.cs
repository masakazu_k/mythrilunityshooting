﻿using UnityEngine;

public class EquippableView : EntityView
{
    public EquippableModel equippableModel { get { return model as EquippableModel; } set { model = value; } }
    public override void ApplyModel()
    {
        if (model == null || model.IsDead)
        {
            Destroy(gameObject);
            return;
        }
        transform.position = equippableModel.position;
        transform.rotation = equippableModel.rotation;

        foreach (var elm in GetComponentsInChildren<Renderer>())
            elm.enabled = (equippableModel.owner == null || equippableModel.owner.activeEquip == equippableModel);
    }
}
