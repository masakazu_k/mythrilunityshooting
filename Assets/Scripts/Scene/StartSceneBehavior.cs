﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSceneBehavior : SceneBehavior
{
    private void Start()
    {
        // カーソルロックを解除
        Cursor.lockState = CursorLockMode.None;
    }

    // Update is called once per frame
    private void Update()
    {
        // クリックされたら、次のシーンへ遷移
        if (Input.GetMouseButtonDown(0)) SetNextScene();
    }
}
