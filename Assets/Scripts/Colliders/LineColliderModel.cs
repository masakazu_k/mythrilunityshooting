﻿using UnityEngine;

public class LineColliderModel : ColliderModel
{
    public override Vector3 GetNearestPoint(Vector3 point)
    {
        var enPoint = EncodePoint(point);

        if (0 < enPoint.z && enPoint.z < 1.0f)
        {
            return DecodePoint(new Vector3(0, 0, enPoint.z));
        }
        else
        {
            var origin = Vector3.Distance(point, position);
            var target = Vector3.Distance(point, position + scale);
            return (origin < target) ? position : position + scale;
        }
    }

    public override Vector3 GetReflectionForce(Vector3 point, Vector3 velocity) => Vector3.zero;

    public override bool IsInsidePoint(Vector3 point)
    {
        point = EncodePoint(point);
        return point.x == 0 && point.y == 0 && point.z >= 0 && point.z <= 1.0f;
    }
    public void SetTransform(Vector3 start, Vector3 end)
    {
        var gap = end - start;
        position = start;
        rotation = Quaternion.LookRotation(gap);
        scale.Set(1, 1, gap.magnitude);
    }
}
