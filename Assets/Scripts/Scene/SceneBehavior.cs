﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneBehavior : MonoBehaviour
{
    // 各SceneBehavior取得処理
    public static T GetSceneBehavior<T>()
    {
        return GameObject.Find("Scene").GetComponent<T>();
    }

    // 次のシーンへ遷移する
    public static void SetNextScene(int indexGap = 1)
    {
        // 連続遷移を避ける処理
        if (Time.timeSinceLevelLoad < 0.3f) return;

        // 現在のシーン番号
        var activeSceneIndex = SceneManager.GetActiveScene().buildIndex;
        // 全てのシーン数
        var scenesCount = SceneManager.sceneCountInBuildSettings;

        // 次のシーン番号へ遷移
        SceneManager.LoadScene((activeSceneIndex + indexGap) % scenesCount);
    }
}
