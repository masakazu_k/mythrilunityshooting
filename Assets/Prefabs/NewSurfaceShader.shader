﻿Shader "Custom/NewSurfaceShader" {
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma vertex vert
		#pragma target 3.0

		struct appdata {
			float4 vertex : POSITION;
			half3 normal : NORMAL;
			half4 color : COLOR;
		};

		struct Input {
			float4 vertColor;
		};

		void vert(inout appdata v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			v.vertex = mul(unity_ObjectToWorld, v.vertex);
			o.vertColor = float4((v.vertex.y + 0.5) * 1.5, 0.0, (0.5 - v.vertex.y) * 1.5, 1.0);
		}

		void surf(Input IN, inout SurfaceOutputStandard o) {
			o.Albedo = IN.vertColor.rgb;
		}
		ENDCG
	}
		FallBack "Diffuse"
}
