﻿using UnityEngine;

public class CameraView : EntityView
{
    public CameraModel cameraModel { get { return model as CameraModel; } set { model = value; } }
    public Camera cam; // カメラ

    public override void ApplyModel()
    {
        base.ApplyModel();
        cam.fieldOfView = cameraModel.fov;
    }
}
