﻿using UnityEngine;

public class EquipmentControl
{
    private PlayerModel playerModel;
    public event System.Action OnAction;
    public event System.Action OnReload;

    private enum TYPE { GUN, GRENADE, KNIFE, NULL };
    private TYPE activeEquipKind = TYPE.NULL;

    public void SetPlayerModel(PlayerModel model)
    {
        playerModel = model;
        playerModel.OnChangeEquip += OnChangeEquip;
        OnChangeEquip(model.activeEquip, null);
    }

    private void OnChangeEquip(EquippableModel newEquip, EquippableModel oldEquip)
    {
        if (newEquip == null) activeEquipKind = TYPE.NULL;
        else if (newEquip is GunModel) activeEquipKind = TYPE.GUN;
        else if (newEquip is GrenadeModel) activeEquipKind = TYPE.GRENADE;
        else if (newEquip is KnifeModel) activeEquipKind = TYPE.KNIFE;
    }

    public void Update()
    {
        switch (activeEquipKind)
        {
            case TYPE.GUN:
                if ((playerModel.activeEquip as GunModel).recastTime < 1.0f ? Input.GetMouseButton(0) : Input.GetMouseButtonDown(0))
                    OnAction?.Invoke();
                if (Input.GetKey("r"))
                    OnReload?.Invoke();
                break;
            case TYPE.GRENADE:
                if (Input.GetMouseButtonUp(0)) OnAction?.Invoke();
                break;
            case TYPE.KNIFE:
                if (Input.GetMouseButtonDown(0)) OnAction?.Invoke();
                break;
        }
    }
}
