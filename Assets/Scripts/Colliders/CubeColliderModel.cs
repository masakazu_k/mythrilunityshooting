﻿using UnityEngine;

public class CubeColliderModel : ColliderModel
{
    private static float GetDistanceAxis(float p) => p < 0 ? Mathf.Min(0, 0.5f - p) : Mathf.Max(0, p - 0.5f);
    private static float GetNearestPointAxis(float p) => Mathf.Clamp(p, -0.5f, 0.5f);
    private static float Hypot(params float[] arg)
    {
        var sum = 0.0f;
        for (var i = 0; i < arg.Length; i++) sum += arg[i] * arg[i];
        return Mathf.Sqrt(sum);
    }

    public override bool IsInsidePoint(Vector3 point)
    {
        point = EncodePoint(point);

        var temp = true;
        for (var i = 0; i < 3; i++) temp &= Mathf.Abs(point[i]) < 0.5f;
        return temp;
    }

    public override float GetDistance(Vector3 point)
    {
        point = EncodePoint(point);
        for (var i = 0; i < 3; i++) point[i] = GetDistanceAxis(point[i]);
        point = DecodePoint(point) - position;

        return Hypot(point[0], point[1], point[2]);
    }

    public override Vector3 GetNearestPoint(Vector3 point)
    {
        point = EncodePoint(point);
        for (var i = 0; i < 3; i++) point[i] = GetNearestPointAxis(point[i]);

        return DecodePoint(point);
    }

    public override Vector3 GetReflectionForce(Vector3 point, Vector3 velocity)
    {
        var enPoint = EncodePoint(point + velocity);

        if (IsInsidePoint(point + velocity))
        {
            var temp = Vector3.zero;
            var maxAxis = Mathf.Max(Mathf.Abs(enPoint[0]), Mathf.Abs(enPoint[1]), Mathf.Abs(enPoint[2]));
            for (var i = 0; i < 3; i++)
                if (maxAxis == Mathf.Abs(enPoint[i]))
                    temp[i] += enPoint[i] < 0 ? -0.5f - enPoint[i] : 0.5f - enPoint[i];

            return DecodePoint(temp) - position;
        }
        else
        {
            return Vector3.zero;
        }
    }
}
