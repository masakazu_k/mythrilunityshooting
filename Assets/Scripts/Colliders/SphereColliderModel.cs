﻿using UnityEngine;

public class SphereColliderModel : ColliderModel
{
    public override bool IsInsidePoint(Vector3 point)
    {
        point = EncodePoint(point);

        return point.magnitude < 0.5f;
    }

    public override Vector3 GetNearestPoint(Vector3 point)
    {
        point = EncodePoint(point);

        return DecodePoint(point.magnitude < 0.5f ? point : point.normalized * 0.5f);
    }

    public override Vector3 GetReflectionForce(Vector3 point, Vector3 velocity)
    {
        point = EncodePoint(point + velocity);
        
        var temp = (0.5f > point.magnitude) ? point.normalized * 0.5f - point : Vector3.zero;

        return DecodePoint(temp) - position;
    }
}
