﻿using UnityEngine;

public class BulletModel : EntityModel
{
    private GunModel gun;
    public CharaModel caster;

    // 生存時間[sec]
    private float birthTime;
    private const float suicideTime = 0.02f;

    public override event System.Action<EntityModel> OnDestroy;

    public BulletModel(GunModel gunModel)
    {
        birthTime = Time.timeSinceLevelLoad;
        collider = new LineColliderModel();
        gun = gunModel;
        caster = gun.owner;

        position = caster.lookOrigin;
        rotation = Quaternion.LookRotation((caster is PlayerModel ? (caster as PlayerModel).look : caster.rotation) * (Vector3.forward * 100.0f + Vector3.up * Random.Range(-gunModel.accuracy, gunModel.accuracy) + Vector3.right * Random.Range(-gunModel.accuracy, gunModel.accuracy)));

        (collider as LineColliderModel).SetTransform(position, rotation * Vector3.forward * 1000.0f);
        mass = 1.0f;
    }

    public override void Hit(EntityModel target)
    {
        if (target == caster || (target as BulletModel)?.caster == caster) return;
        if (target is ObstacleModel) (collider as LineColliderModel).SetTransform(position, collider.GetNearestPoint(target.collider.position));
        if (target is CharaModel) Destroy(target);
    }

    public override void Update() {
        if (birthTime + suicideTime < Time.timeSinceLevelLoad) Destroy(null);
    }
}
