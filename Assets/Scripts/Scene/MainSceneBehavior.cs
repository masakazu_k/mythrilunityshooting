using System.Collections.Generic;
using UnityEngine;

/**
 * 同期処理の見直し -> 完全同期を目指さないで、位置方向と大まかな進行情報
 * -> 当たり判定をクライアントのみで処理して、ダメージイベントを発行して同期する
 * -> クライアントは自プレイヤーと攻撃成功したエンティティに対する処理だけ行う
 * 
 * 敵の種類 -> パラメータで色付け
 * 敵の行動調整 -> 経路探索とかめんどくさそう
 */

public class MainSceneBehavior : SceneBehavior
{
    [SerializeField] private GameObject playerObject;         // プレイヤー
    [SerializeField] private GameObject mainCameraObject;     // カメラ
    [SerializeField] private GameObject enemyObject;          // エネミー
    [SerializeField] private GameObject bulletObject;         // 弾
    [SerializeField] private GameObject smallExplosionObject; // 爆発エフェクト
    [SerializeField] private GameObject largeExplosionObject;
    [SerializeField] private GameObject obstacleObject;       // 障害物
    [SerializeField] private GameObject handGunObject;        // 銃
    [SerializeField] private GameObject subMachinegunObject;
    [SerializeField] private GameObject shotGunObject;
    [SerializeField] private GameObject assaultRifleObject;
    [SerializeField] private GameObject snipperRifleObject;
    [SerializeField] private GameObject grenadeObject;        // グレネード
    [SerializeField] private GameObject knifeObject;          // ナイフ

    private PhotonView photonView;                            // PUN

    private List<EntityModel> entityModels;                   // 全エンティティ
    private List<EntityView> entityViews;

    private CameraModel cameraModel;
    private PlayerModel clientPlayer;
    private EquipmentControl equipControl;

    private const float controllableTime = 4.0f; // 操作可能かどうか
    private bool IsControllable => Time.timeSinceLevelLoad > controllableTime;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();

        entityModels = new List<EntityModel>();
        entityViews = new List<EntityView>();

        var cameraView = mainCameraObject.AddComponent<CameraView>();
        cameraView.cam = mainCameraObject.GetComponent<Camera>();
        cameraModel = cameraView.cameraModel = new CameraModel();
        entityViews.Add(cameraView);
        entityModels.Add(cameraModel);

        equipControl = new EquipmentControl();

        nextEntityId = PhotonNetwork.playerList.Length;
    }

    private int nextEntityId = 0;
    private V CreateEntity<V, M>(GameObject obj, M model) where V : EntityView where M : EntityModel
    {
        var ins = Instantiate(obj);
        var insView = ins.AddComponent<V>();
        if (model != null)
        {
            insView.model = model;
        }

        entityViews.Add(insView);
        if (model != null)
        {
            entityModels.Add(model);
            if (!(model is PlayerModel) && !(model is BulletModel) && !(model is ObstacleModel)) model.id = ++nextEntityId;
        }

        return insView;
    }

    const float fieldSize = 100.0f;
    private void EmitEffect(Vector3 pos) => entityViews.Add(Instantiate(largeExplosionObject, pos, Quaternion.identity).AddComponent<EffectView>());
    private void PlantObstacles(int count = 10)
    {
        var wallPosition = new[]{
            new Vector3(fieldSize, 20.0f, 0.0f),
            new Vector3(-fieldSize, 20.0f, 0.0f),
            new Vector3(0.0f, 20.0f, fieldSize),
            new Vector3(0.0f, 20.0f, -fieldSize),
            new Vector3(0.0f, -20.0f, 0.0f)
        };
        var wallVolume = new[]{
            new Vector3(1.0f, 40.0f, fieldSize * 2.0f),
            new Vector3(1.0f, 40.0f, fieldSize * 2.0f),
            new Vector3(fieldSize * 2.0f, 40.0f, 1.0f),
            new Vector3(fieldSize * 2.0f, 40.0f, 1.0f),
            new Vector3(fieldSize * 2.0f, 40.0f, fieldSize * 2.0f)
        };

        for (var i = 0; i < count + 5; i++)
        {
            var model = new ObstacleModel(fieldSize);
            CreateEntity<ObstacleView, ObstacleModel>(obstacleObject, model);

            if (i < 5)
            {
                model.collider.position = wallPosition[i];
                model.collider.scale = wallVolume[i];
                model.collider.rotation = Quaternion.identity;
            }
        }
    }
    private void SpawnEnemy()
    {
        if (entityModels.FindAll(e => e is EnemyModel).Count > 10 * PhotonNetwork.playerList.Length || Random.value > 0.01f) return;

        // 生成
        var model = new EnemyModel(fieldSize);
        CreateEntity<EnemyView, EnemyModel>(enemyObject, model);
        model.SearchNearestPlayer(entityModels.FindAll(e => e is PlayerModel).ToArray());
        model.targetEntity.OnDestroy += (caused) => model.SearchNearestPlayer(entityModels.FindAll(e => !e.IsDead && e is PlayerModel).ToArray());
        model.OnDestroy += (caused) =>
        {
            photonView.RPC("SendDestroy", PhotonTargets.OthersBuffered, model.id, caused?.id ?? -1);
            EmitEffect(model.collider.position);
        };

        CreateGun(model);
    }
    private void ShotBullet(BulletModel caster) => CreateEntity<BulletView, BulletModel>(bulletObject, caster);

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; // カーソルをロックする

        System.Array.Sort(PhotonNetwork.playerList, (a, b) => a.ID - b.ID);
        foreach (var player in PhotonNetwork.playerList) CreatePlayer(player.ID);
        clientPlayer = entityModels.Find(p => p.id == PhotonNetwork.player.ID) as PlayerModel;
        cameraModel.targetEntity = clientPlayer;
        equipControl.SetPlayerModel(clientPlayer);

        clientPlayer.OnPick += (picked) => photonView.RPC("SendPick", PhotonTargets.OthersBuffered, PhotonNetwork.player.ID, picked.id);

        var ui = GetComponent<UIView>();
        ui.playerModel = clientPlayer;
        //ui.scoreModel = clientPlayer.score;
        ui.scoreModel = new ScoreModel(clientPlayer, entityModels);
        entityViews.Add(ui);

        PlantObstacles(20);
    }

    public bool IsOutOfWorld(Vector3 point) => (Mathf.Abs(point.x) > fieldSize || Mathf.Abs(point.y - (fieldSize - 1.0f)) > fieldSize || Mathf.Abs(point.z) > fieldSize);

    [PunRPC]
    private void SendTransform(int id, Vector3 pos, Quaternion look, int life)
    {
        var entity = entityModels.Find(p => p.id == id) as PlayerModel;
        if (entity == null) return;

        entity.collider.position = pos;
        entity.look = look;
        entity.life = life;
    }

    public enum SEND_KEY_MAP
    {
        W = 1 << 0,
        A = 1 << 1,
        S = 1 << 2,
        D = 1 << 3,
        Q = 1 << 4,
        E = 1 << 5,
        R = 1 << 6,
        SPACE = 1 << 7,
        L_CLICK = 1 << 8,
        R_CLICK = 1 << 9,
    }
    private int GetKeyMap() =>
        (Input.GetKey(KeyCode.W) ? (int)SEND_KEY_MAP.W : 0) |
        (Input.GetKey(KeyCode.A) ? (int)SEND_KEY_MAP.A : 0) |
        (Input.GetKey(KeyCode.S) ? (int)SEND_KEY_MAP.S : 0) |
        (Input.GetKey(KeyCode.D) ? (int)SEND_KEY_MAP.D : 0) |
        (Input.GetKeyDown(KeyCode.Q) ? (int)SEND_KEY_MAP.Q : 0) |
        (Input.GetKeyDown(KeyCode.E) ? (int)SEND_KEY_MAP.E : 0) |
        (Input.GetKeyDown(KeyCode.R) ? (int)SEND_KEY_MAP.R : 0) |
        (Input.GetKeyDown(KeyCode.Space) ? (int)SEND_KEY_MAP.SPACE : 0) |
        (Input.GetMouseButton(0) ? (int)SEND_KEY_MAP.L_CLICK : 0) |
        (Input.GetMouseButtonDown(1) ? (int)SEND_KEY_MAP.R_CLICK : 0);
    [PunRPC]
    private void SendControl(int id, int keyMap, float wheel, Vector2 mouseMove)
    {
        var player = entityModels.Find(p => p.id == id) as PlayerModel;
        if (player == null) return;

        var moveDirection = Vector2.zero;
        if ((keyMap & (int)SEND_KEY_MAP.W) != 0) moveDirection += Vector2.up;
        if ((keyMap & (int)SEND_KEY_MAP.A) != 0) moveDirection += Vector2.left;
        if ((keyMap & (int)SEND_KEY_MAP.S) != 0) moveDirection += Vector2.down;
        if ((keyMap & (int)SEND_KEY_MAP.D) != 0) moveDirection += Vector2.right;
        player.Move(moveDirection);

        if ((keyMap & (int)SEND_KEY_MAP.Q) != 0) player.Eject();
        if ((keyMap & (int)SEND_KEY_MAP.E) != 0) player.Pick(entityModels.FindAll(e => e is EquippableModel).ToArray());
        if ((keyMap & (int)SEND_KEY_MAP.R) != 0) (player.activeEquip as GunModel)?.Reload();
        if ((keyMap & (int)SEND_KEY_MAP.SPACE) != 0) player.Jump();
        if ((keyMap & (int)SEND_KEY_MAP.L_CLICK) != 0) player.activeEquip?.Action();
        if ((keyMap & (int)SEND_KEY_MAP.R_CLICK) != 0) player.isFirstView = !player.isFirstView;

        if (wheel > 0) player.ChangeActiveEquip(player.activeEquipIndex - 1);
        else if (wheel < 0) player.ChangeActiveEquip(player.activeEquipIndex + 1);

        player.MoveLook(mouseMove);
    }

    [PunRPC]
    private void SendDestroy(int id, int causedId)
    {
        var entity = entityModels.Find(p => p.id == id);
        if (entity == null || entity.IsDead) return;

        Debug.Log("Destroy By Other Client");

        entity.Destroy(entityModels.Find(e => e.id == causedId));
    }

    [PunRPC]
    private void SendPick(int id, int equipId)
    {
        var entity = entityModels.Find(p => p.id == id) as PlayerModel;
        var pickEquip = entityModels.Find(p => p.id == equipId) as EquippableModel;
        if (entity == null || pickEquip == null) return;

        entity.PickOne(pickEquip);
    }

    private void CreateGun(CharaModel owner)
    {
        var type = Random.Range(0, 7);
        var objList = new[]
        {
            handGunObject,
            subMachinegunObject,
            shotGunObject,
            assaultRifleObject,
            snipperRifleObject,
            grenadeObject,
            knifeObject
        };
        if (type < 5)
        {
            var gunModel = new GunModel((GunModel.TYPE)type);
            CreateEntity<GunView, GunModel>(objList[type], gunModel);
            owner.Equip(gunModel);
            gunModel.OnAction += e => CreateEntity<BulletView, BulletModel>(bulletObject, e as BulletModel);
        }
        else if (type == 5)
        {
            var grenadeModel = new GrenadeModel();
            CreateEntity<EquippableView, GrenadeModel>(objList[type], grenadeModel);
            owner.Equip(grenadeModel);
            grenadeModel.OnAction += e => CreateEntity<EquippableView, GrenadeModel>(grenadeObject, e as GrenadeModel);
            grenadeModel.OnExplosion += p => EmitEffect(p);
        }
        else if (type == 6)
        {
            var knifeModel = new KnifeModel();
            CreateEntity<EquippableView, KnifeModel>(objList[type], knifeModel);
            owner.Equip(knifeModel);
            knifeModel.OnAction += e => entityModels.Add(e);
        }
    }

    private void CreatePlayer(int id)
    {
        var newEntityModel = new PlayerModel();
        var newEntityView = CreateEntity<PlayerView, PlayerModel>(playerObject, newEntityModel);
        newEntityView.cameraModel = cameraModel;
        newEntityModel.id = id;
        newEntityModel.collider.position = new Vector3(Random.Range(-50.0f, 50.0f), 50.0f, Random.Range(-50.0f, 50.0f));
        newEntityModel.collider.rotation = newEntityModel.look = Quaternion.AngleAxis(Random.Range(-180.0f, 180.0f), Vector3.up);

        CreateGun(newEntityModel);
    }

    private void FixedUpdate()
    {
        SpawnEnemy();

        entityModels.RemoveAll(e => e == null || e.IsDead);
        entityViews.RemoveAll(e => e == null);

        if (IsControllable)
        {
            equipControl.Update();
            photonView.RPC("SendControl", PhotonTargets.AllBuffered, PhotonNetwork.player.ID, GetKeyMap(), Input.GetAxis("Mouse ScrollWheel"), new Vector2(Input.GetAxis("Mouse X") * 0.1f, Input.GetAxis("Mouse Y") * -0.1f));
            photonView.RPC("SendTransform", PhotonTargets.Others, PhotonNetwork.player.ID, clientPlayer.collider.position, clientPlayer.look, clientPlayer.life);
        }

        // エンティティモデル更新処理
        foreach (var elm in entityModels.ToArray())
        {
            elm.Update();
            if (IsOutOfWorld(elm.position)) elm.Destroy(null);
        }
        UpdateCollision();

        foreach (var elm in entityViews.ToArray()) elm.ApplyModel();

        if (clientPlayer.IsDead) SetNextScene();
    }

    private void UpdateCollision()
    {
        var mod = new EntityModel[2];
        var col = new ColliderModel[2];

        // 全エンティティ当たり判定処理
        for (var i = 0; i < entityModels.Count; i++)
        {
            if (entityModels[i] is CameraModel || (entityModels[i] as EquippableModel)?.owner != null) continue;
            mod[0] = entityModels[i];
            col[0] = mod[0].collider;

            for (var j = i + 1; j < entityModels.Count; j++)
            {
                mod[1] = entityModels[j];
                col[1] = mod[1].collider;

                if (col[0].IsHitColliders(col[1]))
                {
                    if (!(mod[0] is ObstacleModel || mod[1] is ObstacleModel)) Debug.Log($"HIT {mod[0]} {mod[1]}");
                    mod[0].Hit(mod[1]);
                    mod[1].Hit(mod[0]);
                }
            }
        }
    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer other) => entityModels.Find(e => e.id == other.ID)?.Destroy(null);
}
