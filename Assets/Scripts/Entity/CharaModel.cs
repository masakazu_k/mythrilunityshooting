﻿using UnityEngine;

public abstract class CharaModel : EntityModel
{
    public bool isFirstView;
    public virtual Vector3 lookOrigin => collider.position + Vector3.up * collider.scale.y * 0.35f;
    public virtual Quaternion look { get; set; }
    public int equipMax { get { return equip.Length; } set { System.Array.Resize(ref equip, value); } }
    public EquippableModel[] equip;
    public int activeEquipIndex = 0;
    public EquippableModel activeEquip { get { return equip[activeEquipIndex]; } set { equip[activeEquipIndex] = value; } }

    public event System.Action<EntityModel, int, bool> OnDamage;
    public override event System.Action<EntityModel> OnDestroy;
    public event System.Action<EquippableModel, EquippableModel> OnChangeEquip;

    public int[] magagines = new int[System.Enum.GetValues(typeof(GunModel.TYPE)).Length];

    public CharaModel()
    {
        collider = new SphereColliderModel();
        mass = 1.5f;
        collider.scale.Set(0.5f, 1.6f, 0.5f);
        equip = new EquippableModel[3];
    }

    public override void Update()
    {
        UpdatePosition();
        if (new Vector3(velocity.x, 0, velocity.z).magnitude > 0) Shake();
    }

    public void UpdateLook()
    {
        look = Quaternion.Euler(Mathf.Clamp((look.eulerAngles.x + 90.0f) % 360.0f, 10.0f, 170.0f) - 90.0f, look.eulerAngles.y, 0);
    }

    public void MoveLook(Vector2 axisGap)
    {
        // スコープ倍率によって視線の振り速度を変える
        if (isFirstView && activeEquip is GunModel) axisGap /= (activeEquip as GunModel).scopeLevel;
        look = Quaternion.AngleAxis(axisGap.x * Mathf.Rad2Deg, Vector3.up) * Quaternion.AngleAxis(axisGap.y * Mathf.Rad2Deg, look * Vector3.right) * look;
        UpdateLook();
    }

    private void Shake()
    {
        look =
            Quaternion.AngleAxis(0.05f * Mathf.Sin(Time.timeSinceLevelLoad * 2.2f), Vector3.up) *
            Quaternion.AngleAxis(0.1f * Mathf.Sin(Time.timeSinceLevelLoad * 2.0f), look * Vector3.right) *
            look;
    }

    public void Equip(EquippableModel equippableModel)
    {
        OnChangeEquip?.Invoke(equippableModel, activeEquip);
        Eject();
        equippableModel.Equip(this);
        activeEquip = equippableModel;
    }

    public virtual EquippableModel Eject()
    {
        if (activeEquip == null) return null;

        OnChangeEquip?.Invoke(null, activeEquip);
        var temp = activeEquip;
        activeEquip.Eject();
        activeEquip = null;

        return temp;
    }

    public void ChangeActiveEquip(int? index)
    {
        if (index == null) index = activeEquipIndex + 1;
        while (index < 0) index += equipMax;
        index %= equipMax;

        OnChangeEquip?.Invoke(equip[(int)index], activeEquip);
        activeEquipIndex = (int)index;
    }

    // 最大HP
    public int lifeMax = 5;
    // HP
    public int life = 5;

    // 比率でHPを取得
    public float GetLifeRate() => life / Mathf.Max(lifeMax, 1);
    // HPが満タンか
    public bool IsFullLife() => life >= lifeMax;
    // 死亡フラグ
    public override bool IsDead => life <= 0;
    // 死亡処理
    public override void Destroy(EntityModel causedEntity = null)
    {
        life = 0;
        DeadBy = causedEntity;
        OnDestroy?.Invoke(causedEntity);
        foreach (var elm in equip) elm?.Eject();
    }
    // ダメージ処理
    public int Damage(EntityModel causedEntity, int value = 1)
    {
        // 最低1は受ける
        life -= Mathf.Max(value, 1);

        OnDamage?.Invoke(causedEntity, value, IsDead);
        if (IsDead) Destroy(causedEntity);

        return life;
    }
}
