﻿using UnityEngine;

/**
 * 複製して、残弾が無ければEject
 */
public class GrenadeModel : EquippableModel
{
    public override event System.Action<EntityModel> OnAction;
    public event System.Action<Vector3> OnExplosion;
    public EntityModel caster;

    public int amoRemain = 1;

    public GrenadeModel()
    {
        name = "Grenade";
        mass = 5.0f;
        collider.scale.Set(0.2f, 0.2f, 0.2f);
    }

    public bool isExplosion = false;
    public float activateTime = -1;
    public float graceTime = 3.0f;
    public float explosionTime = 0.5f;

    public void Activate() => activateTime = Time.timeSinceLevelLoad;
    public void ThrowActive()
    {
        if (amoRemain <= 0) return;
        amoRemain--;

        var throwGrenade = new GrenadeModel();
        throwGrenade.graceTime = graceTime;
        throwGrenade.explosionTime = explosionTime;
        throwGrenade.collider.position = position;
        throwGrenade.collider.rotation = rotation;
        throwGrenade.collider.velocity = (owner is PlayerModel ? (owner as PlayerModel).look : owner.rotation) * Vector3.forward * 10.0f + Vector3.up * 10.0f + owner.velocity;
        throwGrenade.Activate();
        throwGrenade.OnExplosion = OnExplosion;
        throwGrenade.caster = owner;
        OnAction?.Invoke(throwGrenade);

        if (amoRemain <= 0)
        {
            owner.Eject();
            Destroy(null);
        }
    }
    private void Explosion() => isExplosion = true;
    public override void Action() => ThrowActive();
    public override void Update()
    {
        if (activateTime > 0)
        {
            if (activateTime + graceTime + explosionTime < Time.timeSinceLevelLoad) Destroy(null);
            if (!isExplosion && activateTime + graceTime < Time.timeSinceLevelLoad)
            {
                Explosion();
                collider.scale *= 50.0f;
                OnExplosion?.Invoke(position);
            }
        }
        if (owner == null)
        {
            if (IsGrounded)
            {
                collider.velocity.y *= -0.8f;
                collider.velocity *= 0.8f;
                if (collider.velocity.y < 1.0f) collider.velocity.y = 0;
            }
            UpdatePosition();
        }
    }

    public override void Hit(EntityModel target) { }
}
