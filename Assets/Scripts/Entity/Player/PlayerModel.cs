﻿using UnityEngine;

public class PlayerModel : CharaModel
{
    public int score;

    // ダメージクールタイム
    private const float damageCoolTime = 1.0f;

    // 移動速度倍率
    private const float moveMagni = 5.0f;
    // ジャンプ速度倍率
    private const float jumpMagni = 7.5f;

    // 前回ダメージを受けた時刻(ダメージクールタイム用)
    private float lastDamagedTime;

    private LineColliderModel pickerCollider;

    public event System.Action<EquippableModel> OnPick;

    public PlayerModel()
    {
        look = Quaternion.identity;
        score = 0;

        collider.position = Vector3.up * 10.0f;
        lifeMax = 5;
        life = 5;

        lastDamagedTime = Time.timeSinceLevelLoad;

        pickerCollider = new LineColliderModel();
    }

    public override void Update()
    {
        Debug.DrawLine(collider.position, collider.position + collider.rotation * Vector3.forward * 3.0f, Color.red);
        Debug.DrawLine(lookOrigin, lookOrigin + look * Vector3.forward * 3.0f, Color.blue);
        base.Update();

        // 動いている時のみ回転を適用する
        if (collider.velocity.magnitude > Mathf.Abs(collider.velocity.y))
            Rotate(Quaternion.LookRotation(collider.velocity).eulerAngles.y);
        foreach (var elm in equip) elm?.Update();
    }

    public void Move(Vector2 axis)
    {
        axis = Quaternion.Euler(0, 0, -look.eulerAngles.y) * axis;

        if (axis.magnitude == 0)
        {
            collider.velocity.x = collider.velocity.z = 0;
        }
        else
        {
            collider.velocity.x = (axis.normalized * moveMagni).x;
            collider.velocity.z = (axis.normalized * moveMagni).y;
        }
    }

    public void Jump()
    {
        if (IsGrounded) collider.velocity.y = jumpMagni;
    }

    public void Rotate(float direction)
    {
        // 回転速度 10°
        const float speed = 10.0f;
        // 目標角度との差分
        var gap = direction - collider.rotation.eulerAngles.y;
        // 目標角度に指定速度で近付く
        collider.rotation.SetLookRotation(
            Quaternion.Euler(
                0,
                Mathf.Abs(Mathf.Abs(gap) - 180.0f) >= 180.0f - speed
                    ? direction
                    : collider.rotation.eulerAngles.y + speed * (
                        ((Mathf.Abs(gap) < 180.0f) == (gap > 0))
                        ? 1
                        : -1
                    ),
                0
            ) * Vector3.forward
        );
    }

    // ダメージ処理
    public bool Damage()
    {
        // 連続ダメージ回避の為のダメージクールタイムの適用
        if (Time.timeSinceLevelLoad - lastDamagedTime < damageCoolTime) return false;

        lastDamagedTime = Time.timeSinceLevelLoad;
        life--;

        if (IsDead) Destroy(null);

        return true;
    }

    public void Pick(EntityModel[] equippables)
    {
        pickerCollider.SetTransform(lookOrigin, look * Vector3.forward * 3.0f + lookOrigin);
        foreach (EquippableModel elm in equippables)
        {
            if (pickerCollider.IsHitColliders(elm.collider))
            {
                PickOne(elm);
                break;
            }
        }
    }

    public void PickOne(EquippableModel equippable)
    {
        if (
            (equippable.owner != null) ||
            (equippable is GrenadeModel && (equippable as GrenadeModel)?.activateTime != -1)
        ) return;

        OnPick?.Invoke(equippable);

        if (activeEquip == null)
        {
            Equip(equippable);
        }
        else if ((equippable as GunModel)?.type == (activeEquip as GunModel)?.type && (equippable as GunModel)?.amoRemain > 0)
        {
            equippable.Destroy(null);
            magagines[(int)(activeEquip as GunModel).type]++;
        }
        else if ((equippable as GrenadeModel)?.activateTime == -1 && activeEquip is GrenadeModel)
        {
            equippable.Destroy(null);
            (activeEquip as GrenadeModel).amoRemain++;
        }
        else
        {
            Eject();
            Equip(equippable);
        }
    }

    public override void Hit(EntityModel target)
    {
        if ((target as EquippableModel)?.owner == null && (target is CharaModel || target is ObstacleModel)) base.Hit(target);
        if (
            target is EnemyModel ||
            ((target as BulletModel)?.caster != null && (target as BulletModel)?.caster != this) ||
            (target as GrenadeModel)?.isExplosion == true ||
            ((target as KnifeModel)?.caster != null && (target as KnifeModel)?.caster != this)
        ) Damage();
    }
}
