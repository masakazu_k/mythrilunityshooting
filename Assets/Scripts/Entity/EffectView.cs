﻿using UnityEngine;

public class EffectView : CharaView
{
    // エフェクトデータ
	private ParticleSystem particle;

    // Use this for initialization
    private void Start ()
	{
		particle = GetComponent<ParticleSystem>();
	}

	public override void ApplyModel () {
        // 生成から3秒後に破棄
		if (particle != null && particle.time > 3.0f) Destroy(gameObject);
	}
}
