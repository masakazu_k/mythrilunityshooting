﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerView : CharaView
{
    public PlayerModel playerModel { get { return charaModel as PlayerModel; } set { charaModel = value; } }
    public CameraModel cameraModel;

    private Animator animator;
    private Text scoreText;
    private int beforeLife;

    // Animatorキー
    private const string ANIM_KEY_IS_RUNNING = "isRunning";
    private const string ANIM_KEY_IS_JUMP = "isJump";
    private const string ANIM_KEY_IS_DAMAGED = "isDamaged";

    private void Awake()
    {
        animator = GetComponent<Animator>();
        scoreText = transform.Find("Canvas/PlayerScore").GetComponent<Text>();
    }

    // Use this for initialization
    private void Start()
    {
        beforeLife = playerModel.life;
    }

    public override void ApplyModel()
    {
        base.ApplyModel();

        scoreText.text = playerModel.score + " pts.";

        if (model.id == PhotonNetwork.player.ID)
            foreach (var elm in GetComponentsInChildren<Renderer>())
                elm.shadowCastingMode = (!playerModel.isFirstView ? UnityEngine.Rendering.ShadowCastingMode.On : UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly);

        if (animator.isActiveAndEnabled)
        {
            // 接地していなければ、ジャンプモーションを適用する
            animator.SetBool(ANIM_KEY_IS_JUMP, !playerModel.IsGrounded);
            // XZ平面で移動処理が発生していれば、走りモーションを適用する
            animator.SetBool(ANIM_KEY_IS_RUNNING, playerModel.collider.velocity.magnitude > 0.0f && playerModel.collider.velocity.y == 0.0f);
            // ダメージを受けたモーションを適用する
            if (playerModel.life != beforeLife) animator.SetTrigger(ANIM_KEY_IS_DAMAGED);
        }
        beforeLife = playerModel.life;
    }
}
