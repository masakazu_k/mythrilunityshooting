﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Easing
{
    public enum TYPE_CURVE { LINEAR, POWER };
    public enum TYPE_INOUT { IN, OUT, INOUT, OUTIN };

    private static float GetRawEasing(float progress, TYPE_CURVE curve = TYPE_CURVE.LINEAR, float multiplier = 1.0f)
    {
        switch (curve)
        {
            case TYPE_CURVE.LINEAR:
                return progress;
            case TYPE_CURVE.POWER:
                return Mathf.Pow(progress, multiplier);
            default:
                return progress;
        }
    }

    private static float GetEasing(float progress, TYPE_INOUT inout = TYPE_INOUT.IN, TYPE_CURVE curve = TYPE_CURVE.LINEAR, float multiplier = 1.0f)
    {
        switch (inout)
        {
            case TYPE_INOUT.IN:
                return GetRawEasing(progress, curve);
            case TYPE_INOUT.OUT:
                return 1.0f - GetRawEasing(1.0f - progress, curve);
            case TYPE_INOUT.INOUT:
                return (progress < 0.5f ? GetRawEasing(progress * 2.0f, curve) : 2.0f - GetRawEasing(1.0f - progress * 2.0f, curve)) * 0.5f;
            case TYPE_INOUT.OUTIN:
                return (progress < 0.5f ? 1.0f - GetRawEasing(1.0f - progress * 2.0f, curve) : GetRawEasing(progress * 2.0f, curve) + 1.0f) * 0.5f;
            default:
                return 0.0f;
        }
    }

    private float rangeStart;
    private float rangeGap;
    private float startTime;
    private float duringTime;
    private TYPE_CURVE curveType;
    private TYPE_INOUT inoutType;
    private float curveMultiplier;

    public Easing()
    {
        rangeStart = 0.0f;
        rangeGap= 1.0f;

        duringTime = 1.0f;
        inoutType = TYPE_INOUT.INOUT;
        curveType = TYPE_CURVE.LINEAR;
        curveMultiplier = 1.0f;

        startTime = 0.0f;
    }

    public Easing Start()
    {
        startTime = Time.realtimeSinceStartup;

        return this;
    }

    public Easing SetRange(float start, float end)
    {
        rangeStart = start;
        rangeGap = end - start;

        return this;
    }
    public Easing SetDuring(float value)
    {
        if (value != 0.0f) duringTime = value;

        return this;
    }
    public Easing SetInOutType(TYPE_INOUT value)
    {
        inoutType = value;

        return this;
    }
    public Easing SetCurveType(TYPE_CURVE value)
    {
        curveType = value;

        return this;
    }
    public Easing SetCurveMultiplier(float value)
    {
        curveMultiplier = value;

        return this;
    }

    public float GetCurrentValue()
    {
        return GetEasing(
            Mathf.Clamp01((Time.realtimeSinceStartup - startTime) / duringTime),
            inoutType,
            curveType,
            curveMultiplier
        ) * rangeGap + rangeStart;
    }

    public bool IsCompleted => startTime > 0.0f && (Time.realtimeSinceStartup - startTime) > duringTime;
}
