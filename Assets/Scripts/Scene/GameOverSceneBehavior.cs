﻿using UnityEngine;
using UnityEngine.UI;

public class GameOverSceneBehavior : SceneBehavior
{
    // Use this for initialization
    void Start()
    {
        // カーソルロックを解除する
        Cursor.lockState = CursorLockMode.None;
        // スコアを表示する
        GameObject.Find("Score").GetComponent<Text>().text = $"Score: {PlayerPrefs.GetInt("SCORE", 0)}";

        PhotonNetwork.Disconnect();
    }

    // Update is called once per frame
    void Update()
    {
        // クリックされたら、次のシーンへ移行する
        if (Input.GetMouseButtonDown(0)) SetNextScene();
    }
}
