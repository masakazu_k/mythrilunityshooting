﻿using UnityEngine;

/**
 * SGに関してはもう新しいクラスに下ろしていいのでは？
 */
public class GunModel : EquippableModel
{
    public override event System.Action<EntityModel> OnAction;

    public enum TYPE { HG, SM, SG, AR, SR }
    public TYPE type;

    // 最大残弾数
    public int amoMax { get; private set; } = 7;
    // 残弾数
    public int amoRemain { get; private set; } = 7 + 1;
    // 発砲完了時刻
    public float? castCompleteTime { get; private set; } = null;
    // リロード完了時刻
    public float? reloadCompleteTime { get; private set; } = null;
    // リキャストタイム
    public float reloadTime = 3.0f;
    // リコイル
    public float recoilLevel;
    // 弾ブレ
    public float accuracy = 0;

    public GunModel(TYPE _type)
    {
        collider.scale.Set(0.1f, 0.3f, 1.0f);

        type = _type;
        switch (_type)
        {
            case TYPE.HG:
                name = "HandGun";
                amoMax = 7;
                reloadTime = 1.0f;
                recastTime = 0.5f;
                recoilLevel = 10.0f;
                scopeLevel = 1.0f;
                accuracy = 1.0f;
                break;
            case TYPE.SM:
                name = "SubMachineGun";
                amoMax = 120;
                reloadTime = 2.0f;
                recastTime = 0.001f;
                recoilLevel = 2.0f;
                scopeLevel = 0.8f;
                accuracy = 15.0f;
                break;
            case TYPE.SG:
                name = "ShotGun";
                amoMax = 4;
                reloadTime = 3.0f;
                recastTime = 2.0f;
                recoilLevel = 20.0f;
                scopeLevel = 0.6f;
                accuracy = 20.0f;
                break;
            case TYPE.AR:
                name = "AssaultRifle";
                amoMax = 240;
                reloadTime = 4.0f;
                recastTime = 0.05f;
                recoilLevel = 3.0f;
                scopeLevel = 1.2f;
                accuracy = 5.0f;
                break;
            case TYPE.SR:
                name = "SniperRifle";
                amoMax = 12;
                reloadTime = 5.0f;
                recastTime = 3.0f;
                recoilLevel = 15.0f;
                scopeLevel = 4.0f;
                accuracy = 0.01f;
                break;
        }
        amoRemain = amoMax + 1;
    }

    // リロード処理
    public void Reload()
    {
        // リロード可能時間で、Rキーが押されたか、弾を撃ち切っていたら
        if (owner.magagines[(int)type] <= 0 || reloadCompleteTime != null) return;
        // リロード完了時刻を更新
        reloadCompleteTime = Time.timeSinceLevelLoad + reloadTime;
        // リロードキャンセルした時は1発だけ撃てる
        amoRemain = amoRemain > 0 ? 1 : 0;
        // リロードキャンセル時のクールタイムはリロードのクールタイムの半分
        castCompleteTime = Time.timeSinceLevelLoad + reloadTime * 0.5f;
    }

    public bool IsShotable => !(owner == null || castCompleteTime != null || amoRemain <= 0);

    // 発砲処理
    public void Shot()
    {
        if (!IsShotable) return;

        // 残弾があり、クールタイム中じゃないなら
        // リロードをキャンセル
        reloadCompleteTime = null;
        // 残弾数を更新
        amoRemain--;
        // 発砲クールタイムを設定
        castCompleteTime = Time.timeSinceLevelLoad + recastTime;

        // 弾の移動速度を設定
        for (var i = 0; i < (type == TYPE.SG ? 10 : 1); i++)
        {
            var instanceModel = new BulletModel(this);
            if (type == TYPE.SR) instanceModel.mass = 0.0f;

            OnAction?.Invoke(instanceModel);
        }
        Recoil();
    }

    private void Recoil()
    {
        if ((owner as PlayerModel) == null) return;

        owner.look =
            Quaternion.AngleAxis((owner.activeEquip is GunModel) ? -(owner.activeEquip as GunModel).recoilLevel : 0, owner.look * Vector3.right) *
            owner.look;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();

        // リロード完了なら
        if (reloadCompleteTime < Time.timeSinceLevelLoad)
        {
            // リロード可能にする
            reloadCompleteTime = null;
            // 残弾数の更新
            if (owner.magagines[(int)type] > 0)
            {
                amoRemain += amoMax;
                owner.magagines[(int)type]--;
            }
        }
        if (reloadCompleteTime != null && owner.activeEquip != this) reloadCompleteTime = null;
        if (castCompleteTime < Time.timeSinceLevelLoad) castCompleteTime = null;
    }

    public override void Action() => Shot();
}
