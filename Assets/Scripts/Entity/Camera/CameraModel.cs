﻿using UnityEngine;

public class CameraModel : EntityModel
{
    public EntityModel targetEntity;

    public float fov;
    
    // カメラ距離
    private float relativeDist = 4;

    public CameraModel()
    {
        collider = new SphereColliderModel();
    }

    public override void Hit(EntityModel target) { }

    public override void Update()
    {
        var t = targetEntity as PlayerModel;

        if (t.isFirstView)
        {
            collider.position = t.lookOrigin;
            collider.rotation = t.look;
        }
        else
        {
            collider.position = t.lookOrigin + t.look * Vector3.back * relativeDist;
            collider.rotation = t.look;
        }
        fov = t.isFirstView ? 60.0f : 90.0f;
        if (t.isFirstView && ((targetEntity as PlayerModel)?.activeEquip is GunModel)) fov = 60.0f / ((targetEntity as PlayerModel).activeEquip as GunModel).scopeLevel;
    }

    public override event System.Action<EntityModel> OnDestroy;
    public override event System.Action<EntityModel> OnHit;

    public override bool IsDead { get { return false; } protected set { } }
}
