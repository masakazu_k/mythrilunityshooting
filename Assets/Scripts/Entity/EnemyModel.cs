﻿using UnityEngine;

public class EnemyModel : CharaModel
{
    public override Quaternion look => Quaternion.Lerp(rotation, Quaternion.LookRotation(targetEntity.position - position), 0.1f);

    public CharaModel targetEntity;

    // 移動速度倍率
    private float moveMagni;

    // プレイヤーによって破壊された場合に加算されるスコアポイント
    private const int deadScore = 5;

    public EnemyModel(float fieldSize)
    {
        // 移動速度を設定
        moveMagni = 1.0f;

        // 出現位置を設定
        collider.position = new Vector3(Random.Range(-fieldSize, fieldSize), 50.0f, Random.Range(-fieldSize, fieldSize));
    }

    public void SearchNearestPlayer(EntityModel[] playerModels)
    {
        if (playerModels.Length == 0) return;

        var nearestIndex = 0;
        nearestIndex = Random.Range(0, playerModels.Length);
        targetEntity = playerModels[nearestIndex] as CharaModel;
    }

    public override void Update()
    {
        var gap = look * Vector3.forward;
        gap.y = 0;
        collider.rotation.SetLookRotation(gap);

        var g = collider.velocity.y;
        collider.velocity = (gap * moveMagni);
        collider.velocity.y = g;
        UpdatePosition();

        if (activeEquip != null && Vector3.Distance(targetEntity.collider.position, position) < 5.0f * ((activeEquip as GunModel)?.scopeLevel ?? 1.0f)) activeEquip.Action();
    }

    public override void Hit(EntityModel target)
    {
        if (
            target is PlayerModel ||
            ((target as BulletModel)?.caster != null && (target as BulletModel)?.caster != this) ||
            (target as GrenadeModel)?.isExplosion == true ||
            ((target as KnifeModel)?.caster != null && (target as KnifeModel)?.caster != this)
        ) Destroy((target as BulletModel)?.caster ?? target);
        else if ((target as EquippableModel)?.owner == null && (target is CharaModel || target is ObstacleModel)) base.Hit(target);
    }

    public override void Destroy(EntityModel causedEntity)
    {
        base.Destroy(causedEntity);
        if (causedEntity is PlayerModel ||
            (causedEntity as BulletModel)?.caster is PlayerModel ||
            (causedEntity as GrenadeModel)?.caster is PlayerModel ||
            (causedEntity as EquippableModel)?.owner is PlayerModel
        )
            (
                causedEntity as PlayerModel ??
                (causedEntity as BulletModel)?.caster as PlayerModel ??
                (causedEntity as GrenadeModel)?.caster as PlayerModel ??
                (causedEntity as EquippableModel)?.owner as PlayerModel
            ).score += 5;
    }
}
