﻿Shader "Custom/NewSurfaceShader" {
	Properties{
		_BumpMap("Normal Map", 2D) = "bump" {}
	}
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma vertex vert
		#pragma target 3.0

		struct appdata {
			float4 vertex : POSITION;
			half3 normal : NORMAL;
			half4 color : COLOR;
			float4 tangent : TANGENT;
			float4 texcoord : TEXCOORD0;
		};

		struct Input {
			float4 vertColor;
			float2 uv_BumpMap;
		};

		void vert(inout appdata v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.vertColor = float4((v.vertex.y + 0.5) * 0.25 + 0.75, 1.0, 1.0, 1.0);
		}

		sampler2D _BumpMap;
		void surf(Input IN, inout SurfaceOutputStandard o) {
			o.Albedo = IN.vertColor.rgb;
			o.Normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));
		}
		ENDCG
	}
		FallBack "Diffuse"
}
