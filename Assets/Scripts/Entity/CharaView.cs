﻿using UnityEngine;

public abstract class CharaView : EntityView
{
    public virtual CharaModel charaModel { get { return model as CharaModel; } set { model = value; } }
    public override void ApplyModel()
    {
        base.ApplyModel();
        transform.position += Vector3.down * 0.8f;
    }
}
