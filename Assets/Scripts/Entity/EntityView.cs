﻿using UnityEngine;

public abstract class EntityView : MonoBehaviour
{
    public virtual EntityModel model { get; set; }
    public virtual void ApplyModel()
    {
        if (model == null || model.IsDead)
        {
            Destroy(gameObject);
            return;
        }
        transform.position = model.position;
        transform.rotation = model.rotation;
    }
    private void OnDrawGizmos()
    {
        if (model != null)
        {
            Gizmos.color = Color.green;
            Gizmos.matrix = Matrix4x4.Translate(model.collider.position) * Matrix4x4.Rotate(model.rotation) * Matrix4x4.Scale(model.collider.scale);
            if (model.collider is CubeColliderModel) Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
            else if (model.collider is SphereColliderModel) Gizmos.DrawWireSphere(Vector3.zero, 0.5f);
            else if (model.collider is LineColliderModel) Gizmos.DrawLine(Vector3.zero, Vector3.forward);
        }
    }
}
