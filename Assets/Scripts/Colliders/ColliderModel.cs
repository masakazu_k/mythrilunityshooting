﻿using UnityEngine;

public abstract class ColliderModel
{
    public Vector3 position = Vector3.zero;
    public Quaternion rotation = Quaternion.identity;
    public Vector3 scale = Vector3.one;
    public Vector3 velocity = Vector3.zero;
    public bool isMovable = false;

    protected Vector3 EncodePoint(Vector3 point)
    {
        var mtx = Matrix4x4.TRS(position, rotation, scale);

        return mtx.inverse.MultiplyPoint(point);
    }
    protected Vector3 DecodePoint(Vector3 point)
    {
        var mtx = Matrix4x4.TRS(position, rotation, scale);

        return mtx.MultiplyPoint(point);
    }
    public virtual bool IsInsidePoint(Vector3 point) => GetDistance(point) <= 0;
    public virtual float GetDistance(Vector3 point) => Vector3.Distance(point, GetNearestPoint(point));
    public abstract Vector3 GetNearestPoint(Vector3 point);
    public abstract Vector3 GetReflectionForce(Vector3 point, Vector3 velocity);

    public virtual bool IsHitColliders(ColliderModel other) => IsInsidePoint(other.GetNearestPoint(position)) || other.IsInsidePoint(GetNearestPoint(other.position));
    public virtual Vector3 GetReflectionForceColliders(ColliderModel other)
    {
        var nearests = new[] { GetNearestPoint(other.position), other.GetNearestPoint(position) };
        var isInsideIndex = IsInsidePoint(nearests[1]) ? 1 : (other.IsInsidePoint(nearests[0]) ? 0 : -1);

        isInsideIndex = 0;

        switch (isInsideIndex)
        {
            case 1: return GetReflectionForce(nearests[1], other.velocity * 0.016f);
            case 0: return other.GetReflectionForce(nearests[0], velocity * 0.016f);
            case -1: default: return Vector3.zero;
        }
    }
}
